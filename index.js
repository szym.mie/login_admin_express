const EXPRESS = require('express');
const PATH = require('path');
const BODYP = require('body-parser');
const APP = EXPRESS();
const PORT = process.env.PORT || 3000;
//const TIMEOUT = 7200;
const TIMEOUT = 600;

const STATIC = url => `/static/${url}`;
const DIRNAME = url => PATH.join(__dirname, url);
const DOCUMENT = (body, title="") => 
    `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${title}</title>
        <link rel="stylesheet" href="/css">
    </head>
    <body>${body}</body></html>`;

const __USERS__ = [
    {name: "waltar white", password: "1234", age: 55, gender: false, student: false, logon: {T: 0, a: undefined}},
    {name: "czubowna", password: "1234", age: 55, gender: true, student: false, logon: {T: 0, a: undefined}},
    {name: "superankco", password: "1234", age: 5, gender: false, student: true, logon: {T: 0, a: undefined}},
    {name: "nonszalancko", password: "1234", age: 14, gender: true, student: true, logon: {T: 0, a: undefined}}
];

const insertionSort = (arr, prop="name", dir=true) => {
    const l = arr.length;
    for (let i = 1; i < l; i++) {
        c = arr[i], p = i;
        while (p > 0 && arr[p-1][prop] > c[prop]) {
            arr[p--] = arr[p];
        }
        arr[p] = c;
    }
    return dir ? arr : arr.reverse();
}

const existUser = n => __USERS__.some(v => v.name === n);

const addUser = ({name: n, pass: p, age: a, isstu: s, gend: g}) => {
    if (!existUser(n)) {
        __USERS__.push(
            {
                name: n, 
                password: p, 
                age: parseInt(a), 
                student: s == '1' ? true : false, 
                gender: g ? true : false,
                logon: {T: 0, a: undefined}});
        return 0;
    }
    return 1;
}

const logonUser = ({name: n, pass: p}, addr) => {
    const uref = __USERS__[__USERS__.findIndex(v => v.name === n)];
    if (uref !== undefined) {
        if (uref.password === p) {
            if (uref.logon.a === undefined || uref.logon.T <= 0) {
                uref.logon.a = addr;
                uref.logon.T = TIMEOUT;
                return 0;
            }
            return 3;
        }
        return 2;
    }
    return 1;
}

const logoffUser = addr => {
    const i = __USERS__.findIndex(v => v.logon.a === addr);
    if (i !== -1) {
        __USERS__[i].logon.T = 0;
        return 0;
    }
    return 1;
}

const userAccess = addr => __USERS__.findIndex(v => v.logon.a === addr && v.logon.T > 0) !== -1;

const usersTable = users => 
    `<table><tr><th>name</th><th>password</th><th>age</th><th>student?</th><th>gender</th><th>logon: TTL -> addr</th></tr>${users.map(userEntry)}</table>`;

const userEntry = ({name: n, password: p, age: a, student: s, gender: g, logon: {T: t, a: i}}) => 
    `<tr><td>${n}</td><td>${p}</td><td>${a}</td><td>${s}</td><td>${!g ? "m" : "f"}</td><td>${t > 0 ? t : "exp."} -> ${i ? i : "---"}</td></tr>`;

const dirButton = (burl, dir) => 
    `<a href="${burl}?asc=${!dir ? "1" : "0"}"><button>${dir ? "Ascending ↑" : "Descending ↓"}</button></a>`;

const relButton = (burl, dir) =>
    `<a href="${burl}?asc=${dir ? "1" : "0"}"><button>Reload ↺</button></a>`;

const ttlTick = () => {
    __USERS__.map(c => c.logon.T--);
}

setInterval(ttlTick, 1000);

APP.use(EXPRESS.static("static"));
APP.use(BODYP.urlencoded({extended: true}));

APP.get("/", (req, res) => {
    console.log('main');
    res.sendFile(DIRNAME(STATIC(!userAccess(req.ip) ? "main.html" : "main_a.html")));
});

APP.get("/register", (req, res) => {
    res.sendFile(DIRNAME(STATIC(!userAccess(req.ip) ? "register.html" : "register_a.html")))
});

APP.get("/logon", (req, res) => {
    res.sendFile(DIRNAME(STATIC(!userAccess(req.ip) ? "logon.html" : "logon_a.html")))
});

APP.get("/admin", (req, res) => {
    console.log(userAccess(req.ip));
    if (!userAccess(req.ip)) 
        res.redirect("/logon");
    else
        res.sendFile(DIRNAME(STATIC("admin_a.html")));
});

APP.get("/logoff", (req, res) => {
    console.log(logoffUser(req.ip));
    res.redirect("/logon");
});

APP.get("/css", (_, res) => {
    res.sendFile(DIRNAME(STATIC("master.css")))
});

APP.post("/register_action", (req, res) => {
    switch(addUser(req.body)) {
        case 0:
            res.sendFile(DIRNAME(STATIC("reg_suc.html")));
            break;
        case 1:
            res.sendFile(DIRNAME(STATIC("reg_err.html")));
            break;
        default:
            console.error("wrong status code.");
    }
});

APP.post("/logon_action", (req, res) => {
    switch(logonUser(req.body, req.ip)) {
        case 0:
            res.redirect("/admin");
            break;
        case 1:
            res.sendFile(DIRNAME(STATIC("log_nam.html")));
            break;
        case 2:
            res.sendFile(DIRNAME(STATIC("log_pas.html")));
            break;
        case 3:
            res.sendFile(DIRNAME(STATIC("log_dbl.html")));
            break;
        default:
            console.error("wrong status code.");
    }
});

APP.get("/suser", (req, res) => {
    if (!userAccess(req.ip))
        res.redirect("/logon");
    else
        res.send(DOCUMENT(
            dirButton("/suser", req.query.asc == "1")
            +relButton("/suser", req.query.asc == "1")
            +usersTable(insertionSort(__USERS__, "name", req.query.asc == "1"))));
});

APP.get("/sage", (req, res) => {
    if (!userAccess(req.ip))
        res.redirect("/logon");
    else
        res.send(DOCUMENT(
            dirButton("/sage", req.query.asc == "1")
            +relButton("/sage", req.query.asc == "1")
            +usersTable(insertionSort(__USERS__, "age", req.query.asc == "1"))));
});

APP.get("/sgend", (req, res) => {
    if (!userAccess(req.ip))
        res.redirect("/logon");
    else
        res.send(DOCUMENT(
            relButton("/sgend", req.query.asc == "1")
            +usersTable(insertionSort(__USERS__, "gender"))));
});

APP.listen(PORT, () => {});